/*
 * 运行: grunt --gruntfile Grunt_imagemin.js
 */

module.exports = function(grunt) {

    grunt.initConfig({

        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'assets/img/',             // 旧路径
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'assets/dist/'                   // 新路径
                }]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-imagemin');

    grunt.registerTask('default', ['imagemin']);
};




<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>博雅泡泡龙 地图编辑器</title>
    <link rel="stylesheet" href="assets/css/m.css">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
    <style>
    *{ margin: 0; padding: 0; -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;}
    body{ background-color: #fff; color:   #333; text-align: center; background-color: #f2f2f2;}
    #bubblelist{ height: 84px; padding: 5px; width: 602px; background-color: #fff; margin: 20px auto; box-shadow: 1px 1px 3px #aaa;}
    #bubblelist li{ float: left; height: 74px; background-color: #f2f2f2; width: 74px; cursor: pointer; padding-top: 5px; }
    #bubblelist li:hover , #bubblelist li.sel{ background-color: #900;}
        #maps{ width: 256px; margin: 0 auto;}
        #maps li{ list-style: none; width: 100%;}
        #maps li:nth-child(2n){ margin-left: 16px;}
        #maps span{ cursor: pointer; display: block;margin-left: -1px; margin-top: -1px; float: left; width: 32px; height: 32px; border: 1px solid #444; background-color: #fff;}
        #maps span:hover{ background-color: #666;}
        #maps span img{ max-width: 100%; height: auto;}

    input{ width: 40px; color:#322;}
    #lvList{ position: absolute; top: 10px; left: 10px; width:160px; padding: 5px; background-color: #fff; box-shadow: 1px 1px 3px #aaa;}
    #lvList li{float: left; height: 30px; width: 30px;  background-color: #f2f2f2; line-height: 30px; font-size: 20px; cursor: pointer; position: relative;}
    #lvList li i{ position: absolute; top: -10px; right: -15px; height: 20px; width: 20px; background-color: #F55B13; color:#fff; border-radius: 50%; font-size: 12px; line-height: 20px; font-style: normal;display: none; z-index: 100;}
    #lvList li.active ,  #lvList li:hover{ background-color: #900; color: #fff; }
    #lvList li:hover i{ display: inline-block;}

    .time{ width: 200px; margin: 10px auto; padding: 10px; background-color: #fff;  box-shadow: 1px 1px 3px #aaa;}

    .btns{ width: 100%; height: 50px; position: absolute; bottom: 10px; left: 0;}
    .btns button{ width: 41%; height: 50px; margin-left: 6%; text-align: center; border: none; background-color:#067EFF; float: left; box-shadow: 1px 1px 3px #aaa;}
    .btns button:hover{ background-color: #064991;}
    </style>
</head>
<body>

        <ul id="lvList"></ul>
        <div class="time">
            输入时间段
            <label for="i1"><input type="text" id="i1"></label>
            <label for="i2"><input type="text" id="i2"></label>
        </div>

        <ul id="bubblelist">
            <li data-type="b1"><img src="assets/img/bubbles/bubble_1.png" alt=""></li>
            <li data-type="b2"><img src="assets/img/bubbles/bubble_2.png" alt=""></li>
            <li data-type="b3"><img src="assets/img/bubbles/bubble_3.png" alt=""></li>
            <li data-type="b4"><img src="assets/img/bubbles/bubble_4.png" alt=""></li>
            <li data-type="b5"><img src="assets/img/bubbles/bubble_5.png" alt=""></li>
            <li data-type="b6"><img src="assets/img/bubbles/bubble_6.png" alt=""></li>
            <li data-type="b7"><img src="assets/img/bubbles/bubble_7.png" alt=""></li>
            <li data-type="b8"><img src="assets/img/bubbles/bubble_8.png" alt=""></li>
        </ul>

        <ul id="maps">
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
            <li>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                <span></span>
            </li>
        </ul>

        <div class="btns">
            <button id="reset">清除</button>
            <button id="calc">生成</button>
        </div>
</body>
</html>

<script src="assets/js/lib/jquery-2.1.0.min.js"></script>
<script src="assets/js/app/map.js"></script>

<script>
    $(function() {
        var sel = {}, result = {
            star : [],
            map : [
            [],
            [],
            [],
            [],
            [],
            [],
            [],
            [],
            [],[],[],[],[]
        ]
        };

        // init list
        var list = document.getElementById('lvList') , lis = '';
        for (var i = 0, len = Global.lvList.length ; i <= len; i++) {
            if (i == len ) {
                lis += '<li class="active" data-lv='+i+'>'+(i+1)+'</li>';
            }else lis += '<li data-lv='+i+'>'+(i+1)+'<i>x</i></li>';
        }
        list.innerHTML = lis;

        var li_row  = $("#maps li");
        var srcMap = {
            b1 : 'assets/img/bubbles/bubble_1.png',
            b2 : 'assets/img/bubbles/bubble_2.png',
            b3 : 'assets/img/bubbles/bubble_3.png',
            b4 : 'assets/img/bubbles/bubble_4.png',
            b5 : 'assets/img/bubbles/bubble_5.png',
            b6 : 'assets/img/bubbles/bubble_6.png',
            b7 : 'assets/img/bubbles/bubble_7.png',
            b8 : 'assets/img/bubbles/bubble_8.png',
        }
        $("#lvList").on('click','li',function() {
            $("#lvList li").removeClass('active');
            $(this).addClass('active');

            reset();

            // load map
            var lv = $(this).data('lv');
            var data = Global.lvList[lv]
            if (data) {
                var star = data.star;
                $("#i1").val(star[0]);
                $("#i2").val(star[1]);
                for (var i = 0, len = data.map.length ; i < len; i++) {
                    for (var k = 0, len = data.map[i].length ; k < len; k++) {
                        var t = data.map[i][k];
                        if (t) {
                            var span = li_row.eq(i).find('span').eq(k);
                            span.html("<img src='"+srcMap[t]+"'>").data('type',t);
                        }
                    }
                }
            }
        });

        $("#reset").click(reset);
        function reset () {
            // empty map
            $("#maps span").empty().data('type','');
            $("#i1,#i2").val('');
        }

        // remove
        $("#lvList").on('click','i',function() {
            var lv = $(this).parent().data('lv');
            var sure = confirm('删除第'+(lv+1)+'关?');
            if (!sure) return;
            Global.lvList.splice(lv,1);
            $.post('mapEditor.php',{map:JSON.stringify(Global.lvList)},function() {
                window.location.reload();
            });
        });

        $("#bubblelist li").click(function() {
            $("#bubblelist li").removeClass('sel');
            $(this).addClass('sel');
            var type = $(this).data('type'),
                src = $(this).find('img').attr('src');
            sel.type = type;
            sel.src = src;
        });

        $("#maps span").click(function() {
            var type = $(this).data('type');

            if (!sel.type) {
                alert('选择球球');
                return;
            }
            if (type) {
                $(this).empty().data('type','');
            }else{
                var img = new Image();
                img.src = sel.src;
                $(this).data('type',sel.type).append(img);
            }
        });

        $("#calc").click(function() {
            var i1 = $.trim($("#i1").val());
            var i2 = $.trim($("#i2").val());
            var li = $("#lvList .active");
            if (!li) {
                alert('关卡');
                return;
            }
            if (!i1 || !i2) {
                alert('时间段');
                return;
            }
            result.star = [i1,i2];
            $("#maps span").each(function(i) {
                var y = $(this).parent('li').prevAll('li').size();
                var x = $(this).prevAll('span').size();

                var type = $(this).data('type') || null;

                result.map[y][x] = type;
            });

            var lv = li.data('lv');
            Global.lvList[lv] = result;
            $.post('mapEditor.php',{map:JSON.stringify(Global.lvList)},function() {
                window.location.reload();
            });
        });

    });
</script>
<?php

    $map = isset($_POST['map']) ? $_POST['map'] : '';
    if ($map) {
        // 要写入的文件名字
        $filename = 'assets/js/app/map.js';

        $content = "var Global = Global || (window.Global={}) ;Global.lvList=".$map;

        $fh = fopen($filename, "w");
        fwrite($fh, $content);    // 输出：6
        fclose($fh);
    }

?>

var Bubble = function(opt) {
	this.data = _.extend({}, opt);

	this.map = this.data.map;
	this.data.posInfo = this.posInfo;
	// 碰撞的泡泡
	this.hitBubbles = [];
	this.throwFix = 0;
	return this.init();
}
Bubble.prototype = {
	init: function() {
		var img = Global.imgSource[this.get('type')];
		var bitmap = new createjs.Bitmap(img);

		bitmap.x = this.get('x');
		bitmap.y = this.get('y');
		bitmap.width = this.get('width');
		bitmap.height = this.get('height');
		bitmap.name = this.data.id = this.get('type') + '_' + bitmap.id;

		this.el = bitmap;
		return this;
	},

	// 碰撞检测
	checkHit: function(evt) {
		var el = this.el;
		var dx = this.get('dx');
		var dy = this.get('dy');
		var delta = evt.params ? evt.params[0].delta / 10 : 1;
		var me = this;
		var compressorOffset = me.get('parent').compressorOffset;

		el.y += dy * delta;
		el.x += dx * delta;

		if (el.y <= compressorOffset) {
			el.y = compressorOffset;
			this.stop();
		}

		if (el.x >= el.parent.width - el.width) {
			dx *= -1;
			el.x = el.parent.width - el.width;
		} else if (el.x <= 0) {
			dx *= -1;
			el.x = 0;
		}
		this.set('dx', dx);

		// 循环检测是否有人跟他相切
		_.each(this.get('bubbleList'), function(b) {
			if (b.el.id != me.el.id && !me._stop) {
				var dd = Math.sqrt(Math.pow(me.el.x - b.el.x, 2) + Math.pow(me.el.y - b.el.y, 2));
				// 碰撞容差
				if (me.get('width') - dd > me.throwFix) {
					me.stop();
					return;
				}
			}
		});
	},

	// 爆炸
	bomb: function(type) {
		// 在兄弟节点删除自己

		var me = this;
		_.each(this.hitBubbles, function(bubble) {
			if (_.isString(bubble)) return;
			var i, b = _.find(bubble.hitBubbles, function(bb, idx) {
					if (!_.isString(bb) && bb.get('id') == me.get('id')) {
						i = idx;
						return true;
					}
				});
			bubble.hitBubbles.splice(i, 1);
		});

		this.map[this.posInfo.yIdx][this.posInfo.xIdx] = null;

		if (type == 'falldown') {
			var y = this.el.y , dy = Math.random() * 45 + 10;
			createjs.Tween.get(this.el).to({
				y: y + dy,
				opacity : 0.2
			}, 200).call(function() {
				me.el.parent.removeChild(me.el);
			});
		} else {
			this.bombAnimate();
		}
	},

	bombAnimate: function(callback) {
		var anim = new createjs.Sprite(this.data.parent.spriteSheet,'bomb');
		var p = this.el.parent, el = this.el;
		p.removeChild(el);
		anim.x = this.el.x - 38;
		anim.y = this.el.y - 38;
		p.addChild(anim);
		anim.on('animationend', function() {
			this.stop();
			p.removeChild(anim);
		});
		anim.play('bomb');
	},

	fallDown: function() {
		this.bomb('falldown');
	},

	// 发射
	shoot: function(x, y, callback) {
		console.log('发射==>', this.get('id'));
		var me = this;

		var myStageX = this.el.parent.x + this.el.x;
		var myStageY = this.el.parent.y + this.el.y;
		var v = this.get('v');

		// calc from center
		var xx = this.get('width') / 2 + myStageX - x;
		var yy = y - myStageY - this.get('width') / 2;
		var angle = Math.atan(yy / xx);

		// 加农炮 角度
		//
		// var a = angle/Math.PI*180;
		// var cannon = this.data.parent.cannon;
		// if (a<0) {
		// 	a = Math.abs(a) - 90;
		// }else{
		// 	a = Math.abs(a - 90);
		// }
		// a += 180;

		// cannon.rotation = a;
		// a = a/180*Math.PI;

		// var dx = cannon.width*Math.cos( a - Math.PI )/2 + cannon.width/2,
		// 	dy = cannon.width*Math.sin( a - Math.PI )/2;

		// var cx = this.el.parent.parent.canvas.width/2 -41,
		// 	cy = this.el.parent.parent.canvas.height - 80;

		// cannon.x = cx + dx;
		// cannon.y = cy + dy;


		var dx = v * Math.cos(angle) * (angle < 0 ? -1 : 1);
		var dy = -1 * v * Math.abs(Math.sin(angle));

		this.set('dx', dx);
		this.set('dy', dy);

		this.onStopFn = callback;

		this.el.on('tick', function(e) {
			if (me._stop) return;
			me.checkHit.call(me, e);
		});
	},

	stop: function() {
		console.log('stop---', this.get('id'));
		this._stop = true;

		// fix pos
		var me = this;
		var x = this.el.x;
		var y = this.el.y - this.get('parent').compressorOffset;

		// 获取正确位置
		var r = this.checkPos(x, y, this.get('width'));
		this.map[r.yIdx][r.xIdx] = this;

		// 检测旁边的兄弟
		this.checkSiblings(r);

		me.posInfo = r;

		// 兄弟球抖动
		_.each(me.hitBubbles, function(bubble) {
			if (!_.isString(bubble)) {
				bubble.shake();
			}
		});

		// 修正位置 callback
		// 加入下压距离
		createjs.Tween.get(this.el).to({
			x: r.x,
			y: r.y + me.get('parent').compressorOffset
		}, 50).wait(150).call(function() {
			me.onStopFn && me.onStopFn(me);
		});
	},

	checkSiblings: function(r) {
		r = r || this.posInfo;
		var lbb, rbb, tlbb, trbb, blbb, brbb;

		// 检查自己的左右
		lbb = this.map[r.yIdx][r.xIdx - 1];
		if (lbb) {
			_.indexOf(lbb.hitBubbles, this) == -1 && lbb.hitBubbles.push(this);
			_.indexOf(this.hitBubbles, lbb) == -1 && this.hitBubbles.push(lbb);
		}

		rbb = this.map[r.yIdx][r.xIdx + 1];
		if (rbb) {
			_.indexOf(rbb.hitBubbles, this) == -1 && rbb.hitBubbles.push(this);
			_.indexOf(this.hitBubbles, rbb) == -1 && this.hitBubbles.push(rbb);
		}

		// 检查上方
		if (r.yIdx) {
			tlbb = r.yIdx % 2 > 0 ? this.map[r.yIdx - 1][r.xIdx] : this.map[r.yIdx - 1][r.xIdx - 1];
			if (tlbb) {
				_.indexOf(this.hitBubbles, tlbb) == -1 && this.hitBubbles.push(tlbb);
				_.indexOf(tlbb.hitBubbles, this) == -1 && tlbb.hitBubbles.push(this);
			}

			trbb = r.yIdx % 2 > 0 ? this.map[r.yIdx - 1][r.xIdx + 1] : this.map[r.yIdx - 1][r.xIdx];
			if (trbb) {
				_.indexOf(this.hitBubbles, trbb) == -1 && this.hitBubbles.push(trbb);
				_.indexOf(trbb.hitBubbles, this) == -1 && trbb.hitBubbles.push(this);
			}
		} else {
			// 上面是顶
			this.hitBubbles.push('top');
		}

		// 下方
		if (!this.map[r.yIdx + 1]) return;

		blbb = r.yIdx % 2 > 0 ? this.map[r.yIdx + 1][r.xIdx] : this.map[r.yIdx + 1][r.xIdx - 1];
		if (blbb) {
			_.indexOf(this.hitBubbles, blbb) == -1 && this.hitBubbles.push(blbb);
			_.indexOf(blbb.hitBubbles, this) == -1 && blbb.hitBubbles.push(this);
		}

		brbb = r.yIdx % 2 > 0 ? this.map[r.yIdx + 1][r.xIdx + 1] : this.map[r.yIdx + 1][r.xIdx];
		if (brbb) {
			_.indexOf(this.hitBubbles, brbb) == -1 && this.hitBubbles.push(brbb);
			_.indexOf(brbb.hitBubbles, this) == -1 && brbb.hitBubbles.push(this);
		}
	},

	checkPos: function(x, y, w) {
		// 检查在第几行
		var yIdx = Math.round(y / Math.sqrt(3) / w * 2);
		var dx = 0;

		// 奇数行
		if (yIdx % 2 > 0) {
			x -= w / 2;
			dx = w / 2;
		}

		var xIdx = Math.round(x / w);
		// 最右边会超出边界问题
		// TODO 把这个边界写成变量
		if (yIdx % 2) {
			xIdx = xIdx > 6 ? 6 : xIdx;
		}

		return {
			x: xIdx * w + dx,
			y: yIdx * Math.sqrt(3) * w / 2,
			xIdx: xIdx,
			yIdx: yIdx
		}
	},

	shake: function() {
		var y = this.el.y;
		createjs.Tween.get(this.el).to({
			y: y - 10
		}, 70).to({
			y: y
		}, 70);
	},

	get: function(p) {
		return this.data[p];
	},

	set: function(k, v) {
		this.data[k] = v;
	}
}

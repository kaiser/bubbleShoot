var Global = {};

Global.imgManifest = [
    { name:'b1', src : 'bubbles/bubble_1.png'},
    { name:'b2', src : 'bubbles/bubble_2.png'},
    { name:'b3', src : 'bubbles/bubble_3.png'},
    { name:'b4', src : 'bubbles/bubble_4.png'},
    { name:'b5', src : 'bubbles/bubble_5.png'},
    { name:'b6', src : 'bubbles/bubble_6.png'},
    { name:'b7', src : 'bubbles/bubble_7.png'},
    { name:'b8', src : 'bubbles/bubble_8.png'},
    { name:'stone', src : 'bubbles/bubble_9.png'},
    { src : 'page-room.png'},
    { src : 'page-lvlist.png'},
    { src : 'page-idx.png'},
    { src : 'pic.png'},
    { src : 'bg-list.png'},
    { src : 'bg-list-d.png'},
    { src : 'star.png'},
    { src : 'star-b.png'},
    { src : 'next.png'},
    { src : 'prev.png'},
    { name : 'compressor', src : 'compressor.png'},
    { name : 'btn-pause' , src : 'btn-pause.png'},
    { name : 'cannon' , src : 'cannon.png'},
    { name : 'bomb' , src : 'bubbles/bomb.png'}
];

Global.imgSource ={};

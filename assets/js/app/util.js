var Util = {
    formateTime: function(time) {
        var m = Math.floor(time / 60);
        var s = time % 60;
        return m + '′' + s + '″';
    },

    browerKernel: function() {
        var result=null;
        ['webkit', 'moz', 'o', 'ms'].forEach(function(prefix) {
            if (typeof document[prefix + 'Hidden'] != 'undefined') {
                result = prefix;
            }
        });
        return result;
    }
}
